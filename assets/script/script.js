Vue.component('button-counter', {
  data: function () {
    return {
      count: 0,
      data: [],
      cupones: [],
      depositos:[],
    }
  },
  methods: {
    realizarPeticion() {
      let xhttp = new XMLHttpRequest();

      xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          let data = JSON.parse(this.response);
          data.forEach(element => {
            switch(element.type) {
              case 'bonus' :
                this.cupones.push(element);
                break;
              case 'deposit' :
                this.depositos.push(element);
                break;
            }
          });
        }
      };
      xhttp.open("GET", "cupones.json", true);
      xhttp.send();
    },
  },
  created() {
    this.realizarPeticion();
  },
  template: "#cupons"
})


new Vue({ el: '#components-demo' });

